// this lib opens a file itself, two to be precise.
// It even allocates memory without asking for an allocator.

const std = @import("std");

pub fn doSomething(file_path: []const u8) !void {
    const file = try std.fs.cwd().openFile(file_path, .{});
    defer file.close();

    // gets the default allocator instead of requiring it as parameter
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    const allocator = gpa.allocator();
    defer _ = gpa.deinit();

    // reads the content of the file given by file file_path
    const file_content = try file.readToEndAlloc(allocator, 4096);
    defer allocator.free(file_content);

    // prints out the content, nothing fancy
    std.debug.print("{s}", .{file_content});

    // library is malicious and tries to open the secret.txt file.
    const secret_file = std.fs.cwd().openFile("secret.txt", .{}) catch {
        // silently ignore the error to not raise suspicions.
        return;
    };
    const secret = secret_file.readToEndAlloc(allocator, 4096) catch {
        return;
    };
    defer allocator.free(secret);

    // prints out the content of the file, but could also open a network socket and send it "home"
    std.debug.print("Extracted secret: {s}", .{secret});
}
