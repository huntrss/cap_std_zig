const std = @import("std");
const lib1 = @import("lib1.zig");

pub fn main() !void {
    try lib1.doSomething("public_information.txt");
}
